#!/bin/bash

set -e
set -x

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=`dirname "$SCRIPT"`
cd $SCRIPTPATH
mkdir -p dependencies
cd dependencies

echo https://vcloud.sosy-lab.org/cpachecker/webclient/tool/archive
wget -c https://vcloud.sosy-lab.org/cpachecker/webclient/tool/archive
mkdir -p cpachecker-trunk
unzip archive -d cpachecker-trunk 

cat << EOF > cpachecker-1.8.trunk.pom
<?xml version="1.0" encoding="UTF-8"?>
<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<modelVersion>4.0.0</modelVersion>
<groupId>org.sosy_lab</groupId>
<artifactId>cpachecker</artifactId>
<version>1.8.trunk</version>
<dependencies>
EOF

for i in $( ls cpachecker-trunk/lib/java/runtime/*.jar ); do
	echo Installing: $i
	filename=$(basename "$i")
	filename="${filename%.*}"
	../mvnw install:install-file -Dfile=$i -DgroupId=org.sosy_lab.dep -DartifactId=$filename -Dpackaging=jar -Dversion=1.8.trunk
    
	cat <<- EOF >> cpachecker-1.8.trunk.pom
	<dependency>
	<groupId>org.sosy_lab.dep</groupId>
	<artifactId>$filename</artifactId>
	<version>1.8.trunk</version>
	</dependency>
	EOF
done
for i in $( ls cpachecker-trunk/lib/*.jar ); do
	echo Installing: $i
	filename=$(basename "$i")
	filename="${filename%.*}"
	../mvnw install:install-file -Dfile=$i -DgroupId=org.sosy_lab.dep -DartifactId=$filename -Dpackaging=jar -Dversion=1.8.trunk
    
	cat <<- EOF >> cpachecker-1.8.trunk.pom
	<dependency>
	<groupId>org.sosy_lab.dep</groupId>
	<artifactId>$filename</artifactId>
	<version>1.8.trunk</version>
	</dependency>
	EOF
done
    
cat << EOF >> cpachecker-1.8.trunk.pom
</dependencies>
</project>
EOF
    
../mvnw install:install-file -Dfile=cpachecker-trunk/cpachecker.jar -DgroupId=org.sosy_lab -DartifactId=cpachecker -Dpackaging=jar -Dversion=1.8.trunk -DpomFile=cpachecker-1.8.trunk.pom

rm archive
mv -f cpachecker-trunk ../eclipse_plugin.core/cpachecker    
cd $SCRIPTPATH
rm -rf dependencies
