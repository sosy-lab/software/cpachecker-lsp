package org.sosy_lab.cpacheckerlsp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.eclipse.lsp4j.jsonrpc.Launcher;
import org.eclipse.lsp4j.launch.LSPLauncher;
import org.eclipse.lsp4j.services.LanguageClient;

public class CPAcheckerLSP {
  private static CPAcheckerLanguageServer server = null;
  private static ExecutorService executor = null;

  public static void main(String[] args) {

    InputStream in;
    OutputStream out;
    if (args.length == 0) {
    	in = System.in;
    	out = System.out;
    } else {
      int port = Integer.parseInt(args[0]);
      try {
        Socket socket = new Socket("localhost", port);
        in = socket.getInputStream();
        out = socket.getOutputStream();
      } catch (IOException e1) { // TODO Auto-generated catch block
        e1.printStackTrace();
        return;
      }
    }

    Future<?> startListeningFuture = launch(in, out);

    try {
      startListeningFuture.get();
    } catch (InterruptedException e) { // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (ExecutionException e) { // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public static CPAcheckerLanguageServer getServer() {
    return server;
  }

  public static ExecutorService getExecutorService() {
    return executor;
  }

  public static Future<?> launch(InputStream in, OutputStream out) {
    server = new CPAcheckerLanguageServer();
    executor = Executors.newCachedThreadPool();
    Launcher<LanguageClient> launcher = LSPLauncher.createServerLauncher(server, in, out);
    Future<?> startListeningFuture = launcher.startListening();
    server.setRemoteProxy(launcher.getRemoteProxy());
    return startListeningFuture;
  }
}
