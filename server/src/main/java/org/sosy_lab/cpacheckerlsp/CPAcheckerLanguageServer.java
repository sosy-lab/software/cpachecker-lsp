package org.sosy_lab.cpacheckerlsp;

import java.util.concurrent.CompletableFuture;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.lsp4j.CompletionOptions;
import org.eclipse.lsp4j.InitializeParams;
import org.eclipse.lsp4j.InitializeResult;
import org.eclipse.lsp4j.MessageParams;
import org.eclipse.lsp4j.MessageType;
import org.eclipse.lsp4j.ServerCapabilities;
import org.eclipse.lsp4j.TextDocumentSyncKind;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.LanguageServer;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.eclipse.lsp4j.services.WorkspaceService;
import org.sosy_lab.cpacheckerlsp.logging.MessageHandler;

public class CPAcheckerLanguageServer implements LanguageServer {
  private CPAcheckerServices cpacheckerServices;
  LanguageClient client;
  private String workspaceRootUri = null;
  private Logger logger;

  public LanguageClient getClient() {
    return client;
  }

  public String getWorkspaceRootUri() {
    return workspaceRootUri;
  }
  public Logger getLogger() {
	  return this.logger;
  }

  public CPAcheckerLanguageServer() {
    setupLogger();
    cpacheckerServices = new CPAcheckerServices(this);
  }

/**
 * 
 */private void setupLogger(){Handler lspHandler = new MessageHandler(Level.INFO);

logger = Logger.getAnonymousLogger();
logger.setLevel(lspHandler.getLevel());
logger.setUseParentHandlers(false);
logger.addHandler(lspHandler);}

  @Override
  public CompletableFuture<InitializeResult> initialize(InitializeParams params) {
    final InitializeResult res = new InitializeResult(new ServerCapabilities());
    workspaceRootUri = params.getRootUri();
    res.getCapabilities().setColorProvider(true);
    res.getCapabilities().setCompletionProvider(new CompletionOptions());
    res.getCapabilities().setTextDocumentSync(TextDocumentSyncKind.Full);
    return CompletableFuture.supplyAsync(() -> res);
  }

  @Override
  public CompletableFuture<Object> shutdown() {
    return CompletableFuture.supplyAsync(() -> Boolean.TRUE);
  }

  @Override
  public void exit() {}

  @Override
  public TextDocumentService getTextDocumentService() {
    return this.cpacheckerServices;
  }

  @Override
  public WorkspaceService getWorkspaceService() {
    return this.cpacheckerServices;
  }

  public void setRemoteProxy(LanguageClient remoteProxy) {
    this.client = remoteProxy;
  }
}
