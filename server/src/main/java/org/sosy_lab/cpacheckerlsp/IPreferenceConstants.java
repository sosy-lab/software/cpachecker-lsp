package org.sosy_lab.cpacheckerlsp;

import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableSet;

public interface IPreferenceConstants {
  public static final String PREFERENCE_QUALIFIER = "org.sosy_lab.cpalsp.preferences";

  public static final String CPACHECKER_SELECTION = "localorcloud";
  public static final String CPACHECKER_SELECTION_LOCAL = "local";
  public static final String CPACHECKER_SELECTION_CLOUD = "cloud";

  public static final String CPACHECKER_NEW_JAVA_VM = "newjavavm";

  public static final String CPACHECKER_LOCAL_CMDLINE = "cmdline";
  public static final String CPACHECKER_LOCAL_CMDLINE_DEFAULT = "-preprocess";
  public static final String CPACHECKER_SPECIFICATION = "specification";
  public static final String CPACHECKER_SPECIFICATION_DEFAULT = "";
  public static final String CPACHECKER_CONFIGURATION = "configuration";
  public static final String CPACHECKER_CONFIGURATION_DEFAULT = "default";
  
  public static final String CPACHECKER_MACHINEMODEL = "x86orX64";
  public static final String CPACHECKER_MACHINEMODEL_32BIT = "Linux32";
  public static final String CPACHECKER_MACHINEMODEL_64BIT = "Linux64";

  static final ImmutableSet<String> OPTIONS =
      ImmutableSet.of(CPACHECKER_SELECTION, CPACHECKER_LOCAL_CMDLINE, CPACHECKER_SPECIFICATION, CPACHECKER_CONFIGURATION, CPACHECKER_MACHINEMODEL);
  static final ImmutableBiMap<String, String> DEFAULT_OPTIONS =
      ImmutableBiMap.of(
          CPACHECKER_SELECTION,
          CPACHECKER_SELECTION_LOCAL,
          CPACHECKER_LOCAL_CMDLINE,
          CPACHECKER_LOCAL_CMDLINE_DEFAULT,
          CPACHECKER_CONFIGURATION,
          CPACHECKER_CONFIGURATION_DEFAULT,
          CPACHECKER_SPECIFICATION,
          CPACHECKER_SPECIFICATION_DEFAULT,
          CPACHECKER_MACHINEMODEL,
          CPACHECKER_MACHINEMODEL_32BIT);
}
