package org.sosy_lab.cpacheckerlsp;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;

import org.eclipse.lsp4j.ColorInformation;
import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionList;
import org.eclipse.lsp4j.CompletionParams;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DidChangeConfigurationParams;
import org.eclipse.lsp4j.DidChangeTextDocumentParams;
import org.eclipse.lsp4j.DidChangeWatchedFilesParams;
import org.eclipse.lsp4j.DidCloseTextDocumentParams;
import org.eclipse.lsp4j.DidOpenTextDocumentParams;
import org.eclipse.lsp4j.DidSaveTextDocumentParams;
import org.eclipse.lsp4j.DocumentColorParams;
import org.eclipse.lsp4j.MessageParams;
import org.eclipse.lsp4j.MessageType;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.eclipse.lsp4j.services.WorkspaceService;
import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.cpacheckerlsp.server.CpaCheckerManager;
import org.sosy_lab.cpacheckerlsp.server.CpaCheckerManager.CpaLspResults;

public class CPAcheckerServices implements TextDocumentService, WorkspaceService {
  private CPAcheckerLanguageServer langServer;

public CPAcheckerServices(CPAcheckerLanguageServer cpaCheckerLanguageServer) {
	langServer = cpaCheckerLanguageServer;
  }

  @Override
  public CompletableFuture<Either<List<CompletionItem>, CompletionList>> completion(
      CompletionParams position) {

    List<CompletionItem> completionItems = new ArrayList<>();
    completionItems.add(new CompletionItem("Hello World"));

    return CompletableFuture.completedFuture(Either.forLeft(completionItems));
  }

  @Override
  public CompletableFuture<List<ColorInformation>> documentColor(DocumentColorParams params) {
    return null;
  }

  @Override
  public void didOpen(DidOpenTextDocumentParams params) {
    // TODO Auto-generated method stub
    // cpacheckerLanguageServer.getClient().logMessage(new MessageParams(MessageType.Info,
    // "DidOpen"));
  }

  @Override
  public void didChange(DidChangeTextDocumentParams params) {
    // TODO Auto-generated method stub

    // cpacheckerLanguageServer.getClient().logMessage(new MessageParams(MessageType.Info,
    // "DidChange"));
  }

  @Override
  public void didClose(DidCloseTextDocumentParams params) {
    // TODO Auto-generated method stub

    // cpacheckerLanguageServer.getClient().logMessage(new MessageParams(MessageType.Info,
    // "DidClose"));
  }



  @Override
  public void didSave(DidSaveTextDocumentParams params) {

    langServer.getLogger().log(Level.INFO, "Verification started");
    langServer.getClient().showMessage(new MessageParams(MessageType.Info, "Verification started"));
    URI docUri;
    URI workspaceRoot;
    try {
      docUri = new URI(params.getTextDocument().getUri());
      workspaceRoot = new URI(langServer.getWorkspaceRootUri());

    } catch (URISyntaxException e) {
      e.printStackTrace();
      return;
    }
    URI testResultUri = getResultDestination(docUri, workspaceRoot);

    Configuration config = new ServerConfiguration(langServer).getConfiguration();
    CpaLspResults results = null;
    try {
      CpaCheckerManager manager = new CpaCheckerManager(config, docUri, testResultUri);
      results = manager.run();
    } catch (Exception e) {
      e.printStackTrace();
      return;
    }
    if(results==null)
    	return;
    sendResults(results);
  }

  /** @param results */
  private void sendResults(CpaLspResults results) {
    String success = String.format("Verification result: %s", results.getResult());
    langServer.getLogger().log(Level.INFO, success);
    langServer.getClient().showMessage(new MessageParams(MessageType.Info, success));
    if (false == results.getResult()) {
      for (Diagnostic diag : results.getDiagList()) {
        langServer
            .getLogger()
            .log(Level.SEVERE, String.format("Property Violation: %s", diag.getMessage()));
      }
      PublishDiagnosticsParams diagParams = new PublishDiagnosticsParams();
      diagParams.setUri(results.getDocUri().toString());
      diagParams.setDiagnostics(results.getDiagList());
      langServer.getClient().publishDiagnostics(diagParams);
    }
  }

  /**
   * @param docUri
   * @param workspaceRoot
   * @return
   */
  private URI getResultDestination(URI docUri, URI workspaceRoot) {
    URI relativeUri = workspaceRoot.relativize(docUri);
    String relativePath = relativeUri.getPath();
    String testdir = "cparesults/";
    URI testResultUri = workspaceRoot.resolve(testdir + relativePath);
    return testResultUri;
  }

  @Override
  public void didChangeConfiguration(
      DidChangeConfigurationParams params) { // TODO Auto-generated method stub
  }

  @Override
  public void didChangeWatchedFiles(
      DidChangeWatchedFilesParams params) { // TODO Auto-generated method stub
  }
}
