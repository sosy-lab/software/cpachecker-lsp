package org.sosy_lab.cpacheckerlsp;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class LSPClassLoader extends URLClassLoader {
  public LSPClassLoader(URL[] urls) {
    super(urls);
  }

  @SuppressWarnings("resource")
  public static void main(String[] args) {
    List<URL> jarlist = new ArrayList<URL>();
    LSPClassLoader.addJarsInDirectoryToList(jarlist, Paths.get("lib/"));
    LSPClassLoader.addJarsInDirectoryToList(jarlist, Paths.get("cpachecker/"));
    LSPClassLoader.addJarsInDirectoryToList(jarlist, Paths.get("cpachecker/lib"));
    LSPClassLoader.addJarsInDirectoryToList(jarlist, Paths.get("cpachecker/lib/java/runtime"));
    URL[] urls = new URL[jarlist.size()];
    urls = jarlist.toArray(urls);
    LSPClassLoader loader = new LSPClassLoader(urls);
    loader.startCPAcheckerLSP(args);
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  private void startCPAcheckerLSP(String[] args) {
    try {
      Class classToLoad = this.loadClass("org.sosy_lab.cpacheckerlsp.CPAcheckerLSP");
      Method method = classToLoad.getDeclaredMethod("main", String[].class);
      method.invoke(null, (Object) args);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void addJarsInDirectoryToList(List<URL> jarlist, Path path) {
    Predicate<Path> filter =
        p -> {
          boolean res = p.toString().endsWith(".jar");
          // System.out.println(p.toString()+":"+res);
          return res;
        };
    try {
      for (Path jar : Files.list(path).filter(filter).collect(Collectors.toList())) {
        jarlist.add(jar.toUri().toURL());
      }
    } catch (IOException e) { // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  @Override
  public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
    Class<?> loadedClass = findLoadedClass(name);
    if (loadedClass == null) {
      try {
        loadedClass = findClass(name);

        if (resolve) {
          resolveClass(loadedClass);
        }
      } catch (ClassNotFoundException e) {

        loadedClass = super.loadClass(name, resolve);
      }
    }
    return loadedClass;
  }
}
