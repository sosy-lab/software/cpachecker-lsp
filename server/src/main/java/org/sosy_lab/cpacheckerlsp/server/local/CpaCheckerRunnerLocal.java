package org.sosy_lab.cpacheckerlsp.server.local;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.sosy_lab.common.ShutdownManager;
import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.configuration.Option;
import org.sosy_lab.common.configuration.Options;
import org.sosy_lab.cpachecker.cmdline.ConfigurationFactory;
import org.sosy_lab.cpachecker.cmdline.ConfigurationFactory.Config;
import org.sosy_lab.cpachecker.core.CPAchecker;
import org.sosy_lab.cpachecker.core.CPAcheckerResult;
import org.sosy_lab.cpachecker.util.SpecificationProperty;
import org.sosy_lab.cpacheckerlsp.IPreferenceConstants;

import com.google.common.collect.ImmutableMap;

@Options
public class CpaCheckerRunnerLocal {
  Set<SpecificationProperty> properties = null;
  URI docUri = null;
  CPAchecker cpachecker = null;
  ShutdownManager shutdownManager = null;

  @Option(
      name = IPreferenceConstants.CPACHECKER_LOCAL_CMDLINE,
      description = "additional commandline parameters to run cpachecker with")
  String cmdline = null;

  @Option(
      name = IPreferenceConstants.CPACHECKER_CONFIGURATION,
      description = "configuration file name to run cpachecker with")
  private void setConfigFileName(String cpaCheckerConfigFileName) {
	 this.configFileName = cpaCheckerConfigFileName;
  }
  String configFileName = null;
  
  @Option(
	  name = IPreferenceConstants.CPACHECKER_MACHINEMODEL,
	  description = "machine model to run cpachecker with")
	  String machineModel = "Linux32";

  @Option(
      name = IPreferenceConstants.CPACHECKER_SPECIFICATION,
      description = "specification file name to run cpachecker with")
  String specificationFileName = null;
  
  String prepareCmdline() {
	  String cmdline = this.cmdline;
	  if(configFileName != null && 0!= "".compareTo(configFileName)) {
		  cmdline = cmdline + String.format(" -%s", configFileName);
	  }
	  if(specificationFileName != null && 0!= "".compareTo(specificationFileName)) {
		  cmdline = cmdline + String.format(" -spec %s", specificationFileName);
	  }
	  cmdline = cmdline + String.format(" -setprop analysis.machineModel=", machineModel);
	  return cmdline;
  }
  public CpaCheckerRunnerLocal(URI docUri, URI testResultUri, Configuration lsConfig)
      throws InvalidConfigurationException, IOException, InterruptedException {
    // create initial configuration
    // from default values, config file, and command-line arguments

    this.docUri = docUri;
    Map<String, String> options = new HashMap<String, String>();
    properties = new HashSet<SpecificationProperty>();

    lsConfig.inject(this);
    Config config = ConfigurationFactory.create(prepareCmdline(), docUri, testResultUri);
    this.properties = config.properties;

    shutdownManager = ShutdownManager.create();
    cpachecker = new CPAchecker(config.configuration, config.logManager, shutdownManager);
  }

  public CPAcheckerResult run() {
    List<String> programs = new ArrayList<String>();
    programs.add(Paths.get(docUri).toString());
    CPAcheckerResult result = cpachecker.run(programs, properties);
    return result;
  }

  /**
   * The directories where to look for configuration files for options like "-predicateAbstraction"
   * that get translated into a config file name. The directories will be checked in the order they
   * are added here, and the first hit will be taken. Each directory can be mapped to an optional
   * warning that should be shown if the configuration is found there (empty string for no warning).
   */
  private static final ImmutableMap<String, String> DEFAULT_CONFIG_FILES_TEMPLATES =
      ImmutableMap.of(
          "config/%s.properties", "", // no warning
          "config/unmaintained/%s.properties",
              "The configuration %s is unmaintained and may not work correctly.");

  private static @Nullable Path resolveConfigFile(String pName) {
    for (Map.Entry<String, String> template : DEFAULT_CONFIG_FILES_TEMPLATES.entrySet()) {
      Path file = findFile(template.getKey(), pName);
      if (file != null) {
        if (!template.getValue().isEmpty()) {
          // Output.warning(template.getValue(), pName);

        }
        return file;
      }
    }
    return null;
  }
  /**
   * Try to locate a file whose (partial) name is given by the user, using a file name template
   * which is filled with the user given name.
   *
   * <p>If the path is relative, it is first looked up in the current directory, and (if the file
   * does not exist there), it is looked up in the parent directory of the code base.
   *
   * <p>If the file cannot be found, null is returned.
   *
   * @param template The string template for the path.
   * @param name The value for filling in the template.
   * @return An absolute Path object pointing to an existing file or null.
   */
  private static @Nullable Path findFile(final String template, final String name) {
    final String fileName = String.format(template, name);

    Path file = Paths.get(fileName);

    // look in current directory first
    if (Files.exists(file)) {
      return file;
    }

    // look relative to code location second
    Path codeLocation;
    try {
      codeLocation =
          Paths.get(
              CpaCheckerRunnerLocal.class
                  .getProtectionDomain()
                  .getCodeSource()
                  .getLocation()
                  .toURI());
    } catch (SecurityException | URISyntaxException e) {
      // Output.warning("Cannot resolve paths relative to project directory of CPAchecker: %s",
      // e.getMessage());
      return null;
    }
    codeLocation = codeLocation.getParent().getParent();
    codeLocation = codeLocation.resolve("cpachecker");
    file = codeLocation.resolve(fileName);
    if (Files.exists(file)) {
      return file;
    }
    // /home/adrian/eclipse/../git/cpachecker-lsp/eclipse_plugin.core/lib/server.jar
    // /home/adrian/eclipse/../git/cpachecker-lsp/eclipse_plugin.core/lib/config/default.properties
    return null;
  }
}
