package org.sosy_lab.cpacheckerlsp.server.cloud;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.StringJoiner;


public class SubmitRunRequest {
  private URL url;
  private int responseCode;
  private String responseString;

  public SubmitRunRequest() {
    try {
      url = new URL(IVerifierCloudConstants.SUBMIT_RUN);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  public String getResponseString() {
    return responseString;
  }

  public int getResponse() {
    return responseCode;
  }

  public String submit(Map<String, String> data) throws IOException {
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestMethod("POST");
    conn.setDoOutput(true);
    conn.setRequestProperty("User-Agent", IVerifierCloudConstants.USER_AGENT);
    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    conn.setRequestProperty("Accept", "text/plain");
    conn.setRequestProperty("charset", "utf-8");
    conn.setConnectTimeout(15000);
    conn.setReadTimeout(10000);
    OutputStream out = conn.getOutputStream();
    byte[] encodedData = prepareData(data);
    try {
      conn.connect();
      out.write(encodedData);
      responseCode = conn.getResponseCode();
      if (responseCode == HttpURLConnection.HTTP_OK) {
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        StringBuilder sb = new StringBuilder();
        for (int c; (c = in.read()) >= 0; ) sb.append((char) c);
        responseString = sb.toString();
        return responseString;
      }
      else
      {
    	  return null;
      }
    } finally {
      conn.disconnect();
    }
  }

  byte[] prepareData(Map<String, String> data) {
    StringJoiner jAmp = new StringJoiner("&");
    for (Map.Entry<String, String> entry : data.entrySet()) {
      StringJoiner jEquals = new StringJoiner("=");
      try {
        jEquals.add(URLEncoder.encode(entry.getKey(), "UTF-8"));
        jEquals.add(URLEncoder.encode(entry.getValue(), "UTF-8"));
      } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
      }
      jAmp.add(jEquals.toString());
    }
    return jAmp.toString().getBytes();
  }
}
