package org.sosy_lab.cpacheckerlsp.server.cloud;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.common.io.Files;

public class FileUploadRequest {
  private URL url;
  private int response;

  public FileUploadRequest() {
    try {
      url = new URL(IVerifierCloudConstants.UPLOAD_FILES);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  public int getResponse() {
    return response;
  }

  public boolean upload(File file) throws IOException {
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestMethod("POST");
    conn.setDoOutput(true);
    conn.setRequestProperty("User-Agent", IVerifierCloudConstants.USER_AGENT);
    conn.setRequestProperty("Content-Type", "application/octet-stream");
    OutputStream out = conn.getOutputStream();
    Files.copy(file, out);
    response = conn.getResponseCode();
    conn.disconnect();
    return response == HttpURLConnection.HTTP_NO_CONTENT;
  }
}
