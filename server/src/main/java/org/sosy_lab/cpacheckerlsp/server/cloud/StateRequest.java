package org.sosy_lab.cpacheckerlsp.server.cloud;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class StateRequest {
  private URL url;
  private int responseCode;
  private String responseString;

  public StateRequest() {}

  public String getResponseString() {
    return responseString;
  }

  public int getResponse() {
    return responseCode;
  }

  public State submit(String id) throws IOException {

    try {
      String urlString = String.format(IVerifierCloudConstants.GET_STATE, id);
      url = new URL(urlString);
    } catch (MalformedURLException e) {
      throw new IllegalArgumentException(e);
    }
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestMethod("GET");
    conn.setRequestProperty("User-Agent", IVerifierCloudConstants.USER_AGENT);
    conn.setRequestProperty("Accept", "text/plain");
    try {
      responseCode = conn.getResponseCode();
      if (responseCode == HttpURLConnection.HTTP_OK) {
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        StringBuilder sb = new StringBuilder();
        for (int c; (c = in.read()) >= 0; ) sb.append((char) c);
        responseString = sb.toString();
        return State.valueOf(responseString);
      } else {
        return State.REQUESTERROR;
      }
    } finally {
      conn.disconnect();
    }
  }

  public enum State {
    PENDING,
    PROCESSING,
    FINISHED,
    UNKNOWN,
    ERROR,
    REQUESTERROR;
  }
}
