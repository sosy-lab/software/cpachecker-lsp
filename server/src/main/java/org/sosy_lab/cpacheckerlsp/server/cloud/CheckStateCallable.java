package org.sosy_lab.cpacheckerlsp.server.cloud;

import java.util.concurrent.Callable;

import org.sosy_lab.cpacheckerlsp.server.cloud.StateRequest.State;

public class CheckStateCallable implements Callable<StateRequest.State> {
  private String id;

  public CheckStateCallable(String id) {
    this.id = id;
  }

  
  @Override
  public StateRequest.State call() throws Exception {
	StateRequest.State state = State.UNKNOWN;
	do {
		Thread.sleep(500);
		StateRequest stateRequest = new StateRequest();
		state=stateRequest.submit(id);
		
	} while (!isEndState(state));
	
    return state;
  }
  
  private boolean isEndState(StateRequest.State state) {
	  return state == State.ERROR || state == State.FINISHED || state == State.REQUESTERROR;
  }
}
