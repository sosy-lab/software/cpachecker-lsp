package org.sosy_lab.cpacheckerlsp.server.cloud;

public interface IVerifierCloudConstants {
	public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0";
	// https://vcloud.sosy-lab.org/cpachecker/webclient/help/
	public static final String GET_STATE = "https://vcloud.sosy-lab.org/cpachecker/webclient/run/%s/state";
	public static final String GET_RESULT = "https://vcloud.sosy-lab.org/cpachecker/webclient/run/%s/result";
	public static final String UPLOAD_FILES = "https://vcloud.sosy-lab.org/cpachecker/webclient/files/";
	public static final String SUBMIT_RUN = "https://vcloud.sosy-lab.org/cpachecker/webclient/run/";

	public static final String PROGRAM_TEXT_HASH = "programTextHash"; // sha256
	public static final String CONFIGURATION = "configuration";
	public static final String OPTION ="option";
	public static final String PRIORITY = "priority"; //Sets the scheduling priority (URGENT, HIGH, LOW, IDLE, PAUSED).
	public static final String SPECIFICATION = "specification";
}
