package org.sosy_lab.cpacheckerlsp.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.configuration.Option;
import org.sosy_lab.common.configuration.Options;
import org.sosy_lab.cpachecker.core.CPAcheckerResult;
import org.sosy_lab.cpachecker.core.interfaces.Property;
import org.sosy_lab.cpacheckerlsp.IPreferenceConstants;
import org.sosy_lab.cpacheckerlsp.server.cloud.CpaCheckerRunnerCloud;
import org.sosy_lab.cpacheckerlsp.server.local.CpaCheckerRunnerLocal;

@Options
public class CpaCheckerManager {
  private Configuration config;
  private URI docUri;
  private URI testResultUri;
  CpaCheckerRunnerLocal localRunner = null;
  CpaCheckerRunnerCloud cloudRunner = null;

  @Option(
      name = IPreferenceConstants.CPACHECKER_SELECTION,
      description = "the commandline to run cpachecker with")
  private String localOrCloud = IPreferenceConstants.CPACHECKER_SELECTION_LOCAL;

  private boolean useLocalCpaChecker = true;

  public CpaCheckerManager(Configuration config, URI docUri, URI testResultUri)
      throws InvalidConfigurationException, IOException, InterruptedException {
    config.inject(this);
    this.docUri = docUri;
    this.testResultUri = testResultUri;
    useLocalCpaChecker =
        0 == localOrCloud.compareTo(IPreferenceConstants.CPACHECKER_SELECTION_LOCAL);

    if (useLocalCpaChecker) {
      localRunner = new CpaCheckerRunnerLocal(docUri, testResultUri, config);
    } else {
      cloudRunner = new CpaCheckerRunnerCloud(docUri, testResultUri, config);
    }
  }

  public CpaLspResults run() {
    if (useLocalCpaChecker) {
      CPAcheckerResult result = localRunner.run();
      return processCPAcheckerResult(result);
    } else {
      boolean success = cloudRunner.run();
      if (success) {
        return processCPAcheckerResult(testResultUri);
      }
      return null;
    }
  }

  CpaLspResults processCPAcheckerResult(URI testResultUri) {
    Path filepath = Paths.get(testResultUri);
    filepath = Paths.get(filepath.toString(), "output.log");
    File file = filepath.toFile();
    String content = null;
    FileInputStream fis = null;
    try {
      fis = new FileInputStream(file);

    } catch (FileNotFoundException e) { // TODO Auto-generated catch block
      return null;
    }

    try (BufferedReader br = new BufferedReader(new InputStreamReader(fis))) {
      StringBuilder sb = new StringBuilder();
      String line;
      while ((line = br.readLine()) != null) {
        sb.append(line);
        sb.append('\n');
      }
      content = sb.toString();
    } catch (IOException e) {
    }
    Pattern pattern = Pattern.compile("Verification result: (.+?)\\.");
    Matcher matcher = pattern.matcher(content);
    boolean result = false;
    if (matcher.find()) {
      result = Boolean.parseBoolean(matcher.group(1));
    } else {
      return null;
    }
    if (result) {
      return new CpaLspResults(docUri, result, null);
    } else {
      List<Diagnostic> diagList = new ArrayList<Diagnostic>();
      pattern = Pattern.compile("Property violation \\((.+?)\\) found");
      matcher = pattern.matcher(content);
      while (matcher.find()) {
        String violation = matcher.group(1);
        int linenumber = findLineNumbers(violation);
        // "error label in line 13"
        Diagnostic diag =
            new Diagnostic(
                getRangeForLine(linenumber), violation, DiagnosticSeverity.Error, "CPAchecker");
        diagList.add(diag);
      }
      return new CpaLspResults(docUri, result, diagList);
    }
  }

  CpaLspResults processCPAcheckerResult(CPAcheckerResult result) {
    if (result.getResultString().startsWith("TRUE")) {
      return new CpaLspResults(this.docUri, true, null);
    } else {
      Collection<Property> violatedProperties = result.getReached().getViolatedProperties();
      List<Diagnostic> diagList = new ArrayList<Diagnostic>();
      for (Property prop : violatedProperties) {
        int linenumber = findLineNumbers(prop.toString());
        // "error label in line 13"
        Diagnostic diag =
            new Diagnostic(
                getRangeForLine(linenumber),
                prop.toString(),
                DiagnosticSeverity.Error,
                "CPAchecker");
        diagList.add(diag);
      }
      return new CpaLspResults(this.docUri, false, diagList);
    }
  }

  private int findLineNumbers(String string) {
    Pattern p = Pattern.compile("\\d+$");
    Matcher m = p.matcher(string);
    m.find();
    String numberstring = m.group();
    return Integer.parseInt(numberstring);
  }

  private Range getRangeForLine(int linenumber) {
    File document = new File(docUri);
    BufferedReader br;
    List<String> lines = new ArrayList<String>();
    try {
      br = new BufferedReader(new FileReader(document));
      String line;
      line = br.readLine();
      while (line != null) {
    	  lines.add(line);
        line = br.readLine();
      }
      br.close();
    } catch (Exception e) {
    	return new Range(new Position(linenumber - 1, 1), new Position(linenumber - 1, 2));
    }
    if (!lines.isEmpty()) {
    	String line = lines.get(linenumber-1);
    	String filter = "^\\s+";
    	int leadingWhitspaces = line.length()-line.replaceAll(filter, "").length();
    	int trimmedLength=line.trim().length();
    	return new Range(new Position(linenumber - 1, leadingWhitspaces), new Position(linenumber - 1, leadingWhitspaces+trimmedLength));
    }
    return new Range(new Position(linenumber - 1, 1), new Position(linenumber - 1, 2));
    
  }

  public class CpaLspResults {
    // verification result
    private boolean result;
    private URI docUri;
    private List<Diagnostic> diagList;

    public CpaLspResults(URI docUri, boolean result, List<Diagnostic> diagList) {
      this.result = result;
      this.docUri = docUri;
      this.diagList = diagList;
    }

    public boolean getResult() {
      return this.result;
    }

    public List<Diagnostic> getDiagList() {
      return diagList;
    }

    public URI getDocUri() {
      return docUri;
    }
  }
}
