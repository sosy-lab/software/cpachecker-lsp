package org.sosy_lab.cpacheckerlsp.server.cloud;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;


public class ResultDownloadRequest {
  private URL url;
  private int responseCode;
  private String responseString;

  public ResultDownloadRequest() {}

  public String getResponseString() {
    return responseString;
  }

  public int getResponse() {
    return responseCode;
  }

  public void download(String id, File destination) throws IOException {

    try {
      String urlString = String.format(IVerifierCloudConstants.GET_RESULT, id);
      url = new URL(urlString);
    } catch (MalformedURLException e) {
      throw new IllegalArgumentException(e);
    }
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestMethod("GET");
    conn.setRequestProperty("User-Agent", IVerifierCloudConstants.USER_AGENT);
    conn.setRequestProperty("Accept", "application/zip");
    try {
        BufferedInputStream in = new BufferedInputStream(conn.getInputStream());
        Files.copy(in, destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
    } finally {
      conn.disconnect();
    }
  }
}
