package org.sosy_lab.cpacheckerlsp.server.cloud;

import java.net.URI;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.configuration.Option;
import org.sosy_lab.common.configuration.Options;
import org.sosy_lab.common.io.TempFile;
import org.sosy_lab.cpacheckerlsp.CPAcheckerLSP;
import org.sosy_lab.cpacheckerlsp.IPreferenceConstants;
import org.sosy_lab.cpacheckerlsp.server.cloud.StateRequest.State;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

@Options
public class CpaCheckerRunnerCloud {

  URI docUri;
  URI resultDestination;
  String filehash;

  @Option(
	  name = IPreferenceConstants.CPACHECKER_CONFIGURATION,
	  description = "configuration file name to run cpachecker with")
	  String configFileName = "";

  @Option(
	  name = IPreferenceConstants.CPACHECKER_SPECIFICATION,
	  description = "specification file name to run cpachecker with")
	  String specificationFileName = "";
	  
  @Option(
	  name = IPreferenceConstants.CPACHECKER_MACHINEMODEL,
	  description = "machine model to run cpachecker with")
	  String machineModel = "Linux32";


  public CpaCheckerRunnerCloud(URI docUri, URI testResultUri, Configuration lsConfig)
      throws InvalidConfigurationException, IOException {
    this.docUri = docUri;
    resultDestination = testResultUri;
    lsConfig.inject(this);
    filehash = sha256File(docUri);
  }

  public boolean run() {
    FileUploadRequest upload = new FileUploadRequest();
    Logger logger = CPAcheckerLSP.getServer().getLogger();
    logger.log(Level.INFO, "Uploading File to VerifierCloud");
    boolean uploaded = false;
    try {
      uploaded = upload.upload(new File(docUri));
    } catch (IOException e) { // TODO Auto-generated catch block
      e.printStackTrace();
      return false;
    }
    if (!uploaded) {
      return false;
    }
    SubmitRunRequest submit = new SubmitRunRequest();
    Map<String, String> data = new HashMap<String, String>();
    data.put(IVerifierCloudConstants.PROGRAM_TEXT_HASH, filehash);
    data.put(IVerifierCloudConstants.OPTION, "analysis.machineModel="+machineModel);
    if(configFileName.compareTo("")==0 || specificationFileName==null) {
        data.put(IVerifierCloudConstants.CONFIGURATION, "default");
    }else
    {
        data.put(IVerifierCloudConstants.CONFIGURATION, configFileName);
    }
    if(specificationFileName.compareTo("")==0 || specificationFileName==null) {
    	//dont add specification data
    }else
    {
        data.put(IVerifierCloudConstants.SPECIFICATION, specificationFileName);
    }
    String id = null;

    logger.log(Level.INFO, "Submitting run configuration to VerifierCloud");
    try {
      id = submit.submit(data);
    } catch (IOException e) { // TODO Auto-generated catch block
      e.printStackTrace();
      logger.log(Level.INFO, "Submitting run configuration timed out after 15 seconds");
      return false;
      
    }
    CheckStateCallable checkState = new CheckStateCallable(id);
    Future<State> stateFuture = CPAcheckerLSP.getExecutorService().submit(checkState);
    State state = null;
    logger.log(Level.INFO, "Waiting for verification run completion");
    try {
      state = stateFuture.get();
    } catch (InterruptedException | ExecutionException e) { // TODO Auto-generated catch block
      e.printStackTrace();
      return false;
    }
    if (state != State.FINISHED) {
      // TODO: something went wrong
      return false;
    }
    ResultDownloadRequest resultDownload = new ResultDownloadRequest();

    logger.log(Level.INFO, "Downloading results to temporary files");
    File tempFile;
    try {
      tempFile = File.createTempFile(String.format("id-%s-", id), "-result.zip");
      tempFile.deleteOnExit();
    } catch (IOException e) { // TODO Auto-generated catch block
      e.printStackTrace();
      return false;
    }
    try {
      resultDownload.download(id, tempFile);
    } catch (IOException e) { // TODO Auto-generated catch block
      e.printStackTrace();
      return false;
    }

    logger.log(Level.INFO, "Unpacking results");
    try {
      unzip(tempFile, new File(resultDestination));
    } catch (IOException e) { // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return true;
  }

  void unzip(File zipFile, File destination) throws IOException {
    ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
    try {
      ZipEntry zipEntry = zis.getNextEntry();
      while (zipEntry != null) {
    	String entryName = zipEntry.getName();
    	if (entryName.endsWith("/")) {
    		zipEntry = zis.getNextEntry();
    		continue;
    	}
        String fileName = destination.getPath() + File.separator + entryName;
        File file = new File(fileName);
        File parent = file.getParentFile();
        parent.mkdirs();
        Files.copy(zis, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        zipEntry = zis.getNextEntry();
      }
    } finally {
      zis.closeEntry();
      zis.close();
    }
  }

  public static String sha256File(URI fileUri) throws IOException {
    Hasher sha256 = Hashing.sha256().newHasher();
    RandomAccessFile file = new RandomAccessFile(new File(fileUri), "r");
    FileChannel fileChannel = file.getChannel();
    MappedByteBuffer buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
    buffer.load();
    sha256.putBytes(buffer);
    fileChannel.close();
    file.close();
    return sha256.hash().toString();
  }
}
