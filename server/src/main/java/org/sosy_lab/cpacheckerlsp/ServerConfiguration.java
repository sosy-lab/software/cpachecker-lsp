package org.sosy_lab.cpacheckerlsp;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.eclipse.lsp4j.ConfigurationItem;
import org.eclipse.lsp4j.ConfigurationParams;
import org.eclipse.lsp4j.MessageParams;
import org.eclipse.lsp4j.MessageType;
import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.configuration.ConfigurationBuilder;
import org.sosy_lab.common.configuration.InvalidConfigurationException;

public class ServerConfiguration {
  public CPAcheckerLanguageServer langServer;

  public ServerConfiguration(CPAcheckerLanguageServer langServer) {
    this.langServer = langServer;
  }

  public Configuration getConfiguration() {

    try {
      try {
        return getConfigurationFromClient();
      } catch (TimeoutException e) {
        return getConfigurationFromFile();
      }
    } catch (Exception e) {
      langServer
          .getClient()
          .showMessage(
              new MessageParams(
                  MessageType.Error,
                  "Invalid configuration or cpals.cfg not found, using default"));
    }
    return getDefaultConfiguration();
  }

  private Configuration getConfigurationFromClient()
      throws InvalidConfigurationException, InterruptedException, ExecutionException,
          TimeoutException {

    List<ConfigurationItem> configurationItems = prepareConfigurationItemList();

    ConfigurationParams configurationParams = new ConfigurationParams(configurationItems);

    CompletableFuture<List<Object>> configurationFuture =
        langServer.getClient().configuration(configurationParams);

    ConfigurationBuilder configBuilder = Configuration.builder();
    List<Object> configuration = null;
    configuration = configurationFuture.get(2, TimeUnit.SECONDS);
    for (int i = 0; i < configuration.size(); i++) {
      configBuilder.setOption(
          configurationItems.get(i).getSection(), (String) configuration.get(i));
    }
    return configBuilder.build();
  }
  /**
   * @throws InvalidConfigurationException
   * @throws IOException
   */
  private Configuration getConfigurationFromFile()
      throws IOException, InvalidConfigurationException {
    String cfgFile = null;
    try {
      cfgFile = new URI(langServer.getWorkspaceRootUri() + "cpals.cfg").getPath();
    } catch (URISyntaxException e2) {
      langServer
          .getClient()
          .showMessage(new MessageParams(MessageType.Error, e2.getLocalizedMessage()));
    }

    ConfigurationBuilder configBuilder = Configuration.builder();
    configBuilder.loadFromFile(cfgFile);
    return configBuilder.build();
  }

  /** @return */
  private List<ConfigurationItem> prepareConfigurationItemList() {
    List<ConfigurationItem> configurationItems = new ArrayList<ConfigurationItem>();
    for (String item : IPreferenceConstants.OPTIONS) {
      ConfigurationItem configurationItem = new ConfigurationItem();
      configurationItem.setScopeUri(langServer.getWorkspaceRootUri());
      configurationItem.setSection(item);
      configurationItems.add(configurationItem);
    }
    return configurationItems;
  }

  private Configuration getDefaultConfiguration() {

    ConfigurationBuilder configBuilder = Configuration.builder();
    configBuilder.setOptions(IPreferenceConstants.DEFAULT_OPTIONS);
    try {
      Configuration config = configBuilder.build();
      return config;
    } catch (InvalidConfigurationException e) { // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }
}
