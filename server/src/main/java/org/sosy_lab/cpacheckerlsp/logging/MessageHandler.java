package org.sosy_lab.cpacheckerlsp.logging;

import java.util.List;
import java.util.Set;
import java.util.logging.ErrorManager;
import java.util.logging.Filter;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.eclipse.lsp4j.MessageParams;
import org.eclipse.lsp4j.MessageType;
import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.log.LoggingOptions;
import org.sosy_lab.cpacheckerlsp.CPAcheckerLSP;

import com.google.common.collect.ImmutableSet;

public class MessageHandler extends Handler {

  @Override
  public void close() throws SecurityException {}

  @Override
  public void flush() {}

  @Override
  public void publish(LogRecord record) {
    if (!isLoggable(record)) {
      return;
    }
    String message;
    try {
      Formatter formatter = getFormatter();
      if (formatter != null) {
        message = formatter.format(record);
      } else {
        message = record.getMessage();
      }
    } catch (Exception ex) {
      // report the exception to any registered ErrorManager.
      reportError(null, ex, ErrorManager.FORMAT_FAILURE);
      return;
    }

    Level recordlevel = record.getLevel();
    MessageType type;

    switch (recordlevel.intValue()) {
      case 0:
    	  type = MessageType.Error;
        return;
      case 1:
        type = MessageType.Warning;
        break;
      case 2:
        type = MessageType.Info;
        break;
      default:
        type = MessageType.Log;
    }
    MessageParams messageParams = new MessageParams(type, message);
    CPAcheckerLSP.getServer().getClient().logMessage(messageParams);
  }

  public MessageHandler(LoggingOptions logOptions) {
    setupHandler(logOptions);
  }
  public MessageHandler(Level logLevel)
  {
	setLevel(logLevel);
	setFormatter(new LogFormatter());
	setFilter(null);
  }

  void setupHandler(LoggingOptions logOptions) {
    setLevel(logOptions.getConsoleLevel());
    setFormatter(new LogFormatter());
    List<Level> excludeLevels = logOptions.getConsoleExclude();
    if (!excludeLevels.isEmpty()) {
      setFilter(new LogLevelFilter(excludeLevels));
    } else {
      setFilter(null);
    }
  }

  private class LogLevelFilter implements Filter {

    private final Set<Level> excludeLevels;

    LogLevelFilter(List<Level> excludeLevels) {
      this.excludeLevels = ImmutableSet.copyOf(excludeLevels);
    }

    @Override
    public boolean isLoggable(LogRecord pRecord) {
      return !excludeLevels.contains(pRecord.getLevel());
    }
  }
}
