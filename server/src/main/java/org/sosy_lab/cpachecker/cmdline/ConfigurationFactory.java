package org.sosy_lab.cpachecker.cmdline;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.sosy_lab.common.Optionals;
import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.configuration.ConfigurationBuilder;
import org.sosy_lab.common.configuration.FileOption;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.configuration.Option;
import org.sosy_lab.common.configuration.Options;
import org.sosy_lab.common.configuration.FileOption.Type;
import org.sosy_lab.common.configuration.converters.FileTypeConverter;
import org.sosy_lab.common.log.BasicLogManager;
import org.sosy_lab.common.log.FileLogFormatter;
import org.sosy_lab.common.log.LogManager;
import org.sosy_lab.common.log.LoggingOptions;
import org.sosy_lab.cpachecker.cmdline.CmdLineArguments.InvalidCmdlineArgumentException;
import org.sosy_lab.cpachecker.cpa.automaton.AutomatonGraphmlParser;
import org.sosy_lab.cpachecker.cpa.testtargets.TestTargetType;
import org.sosy_lab.cpachecker.util.Property;
import org.sosy_lab.cpachecker.util.PropertyFileParser;
import org.sosy_lab.cpachecker.util.SpecificationProperty;
import org.sosy_lab.cpachecker.util.Property.CommonCoverageType;
import org.sosy_lab.cpachecker.util.Property.CommonPropertyType;
import org.sosy_lab.cpachecker.util.PropertyFileParser.InvalidPropertyFileException;
import org.sosy_lab.cpachecker.util.automaton.AutomatonGraphmlCommon.WitnessType;
import org.sosy_lab.cpachecker.util.globalinfo.GlobalInfo;
import org.sosy_lab.cpacheckerlsp.CPAcheckerLSP;
import org.sosy_lab.cpacheckerlsp.logging.MessageHandler;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.io.MoreFiles;

public class ConfigurationFactory {
  public static Config create(String cmdline, URI docUri, URI testResultUri)
      throws InvalidConfigurationException, IOException, InterruptedException {

    List<String> argsList = new ArrayList<String>();
    Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(cmdline);
    while (m.find()) argsList.add(m.group(1));
    String[] args = new String[argsList.size()];
    args = argsList.toArray(args);
    Map<String, String> cmdLineOptions;
    try {
      cmdLineOptions = CmdLineArguments.processArguments(args);
    } catch (InvalidCmdlineArgumentException e) {
      throw new InvalidConfigurationException(e.getLocalizedMessage());
    }

    boolean secureMode = cmdLineOptions.remove(CmdLineArguments.SECURE_MODE_OPTION) != null;
    if (secureMode) {
      CPAcheckerLSP.getServer().getLogger().log(Level.INFO, "Secure mode is now enabled, and will stay enabled in this CPAcheckerLSP instance");
      Configuration.enableSecureModeGlobally();
    }
    // Read property file if present and adjust cmdline options
    Set<SpecificationProperty> properties;
    try {
      properties = handlePropertyFile(cmdLineOptions);
    } catch (InvalidCmdlineArgumentException e) {
      throw new InvalidConfigurationException(e.getLocalizedMessage());
    }
    // get name of config file (may be null)
    // and remove this from the list of options (it's not a real option)
    String configFile = cmdLineOptions.remove(CmdLineArguments.CONFIGURATION_FILE_OPTION);

    // create initial configuration
    // from default values, config file, and command-line arguments
    ConfigurationBuilder configBuilder = Configuration.builder();
    Map<String, String> defaultOptions = new HashMap<String, String>();

    defaultOptions.putIfAbsent("analysis.programNames", Paths.get(docUri).toString());
    defaultOptions.putIfAbsent("parser.usePreprocessor", "true");
    defaultOptions.putIfAbsent("log.level", Level.INFO.toString());
    defaultOptions.putIfAbsent("output.path", Paths.get(testResultUri).toString());
    configBuilder.setOptions(defaultOptions);
    if (configFile != null) {
      configBuilder.loadFromFile(configFile);
    }

    configBuilder.setOptions(cmdLineOptions);
    Configuration config = configBuilder.build();

    // We want to be able to use options of type "File" with some additional
    // logic provided by FileTypeConverter, so we create such a converter,
    // add it to our Configuration object and to the the map of default converters.
    // The latter will ensure that it is used whenever a Configuration object
    // is created.
    FileTypeConverter fileTypeConverter =
        secureMode
            ? FileTypeConverter.createWithSafePathsOnly(config)
            : FileTypeConverter.create(config);
    String outputDirectory = fileTypeConverter.getOutputDirectory();
    Configuration.getDefaultConverters().put(FileOption.class, fileTypeConverter);

    config =
        Configuration.builder()
            .copyFrom(config)
            .addConverter(FileOption.class, fileTypeConverter)
            .build();
    LoggingOptions logOptions = null;
    LogManager logManager = null;
    logOptions = new LoggingOptions(config);
    Handler lspHandler = new MessageHandler(logOptions);

    Logger logger = Logger.getAnonymousLogger();
    logger.setLevel(lspHandler.getLevel());
    logger.setUseParentHandlers(false);
    logger.addHandler(lspHandler);

    // create file logger
    Path outputFile = logOptions.getOutputFile();
    if (outputFile != null) {
      try {
        MoreFiles.createParentDirectories(outputFile);

        Handler outfileHandler =
            new FileHandler(outputFile.toAbsolutePath().toString(), /*append=*/ false);
        outfileHandler.setFormatter(new FileLogFormatter());
        outfileHandler.setLevel(lspHandler.getLevel());
        outfileHandler.setFilter(lspHandler.getFilter());
        logger.addHandler(outfileHandler);
      } catch (IOException e) {
        logger.log(
            Level.WARNING, "Could not open log file (" + e.getMessage() + "), no file logging");
      }
    }

    logManager = new BasicLogManager(logger, 0);

    GlobalInfo.getInstance().storeLogManager(logManager);
    config.enableLogging(logManager);

    // Read witness file if present, switch to appropriate config and adjust cmdline options
    config = handleWitnessOptions(config, cmdLineOptions);

    BootstrapOptions options = new BootstrapOptions();
    config.inject(options);
    // Switch to appropriate config depending on property (if necessary)
    config = handlePropertyOptions(config, options, cmdLineOptions, properties);

    if (options.printUsedOptions) {
      // TODO: adjust to lsp notifications
      // config.dumpUsedOptionsTo(System.out);
    }
    return new Config(config, outputDirectory, properties, logManager);
  }

  // everything from here on is copied from CPAMain.java

  private static final String SPECIFICATION_OPTION = "specification";
  private static final ImmutableMap<Property, String> SPECIFICATION_FILES =
      ImmutableMap.<Property, String>builder()
          .put(CommonPropertyType.REACHABILITY_LABEL, "sv-comp-errorlabel")
          .put(CommonPropertyType.REACHABILITY, "sv-comp-reachability")
          .put(CommonPropertyType.VALID_FREE, "sv-comp-memorysafety")
          .put(CommonPropertyType.VALID_DEREF, "sv-comp-memorysafety")
          .put(CommonPropertyType.VALID_MEMTRACK, "sv-comp-memorysafety")
          .put(CommonPropertyType.VALID_MEMCLEANUP, "sv-comp-memorycleanup")
          .put(CommonPropertyType.OVERFLOW, "sv-comp-overflow")
          .put(CommonPropertyType.DEADLOCK, "deadlock")
          // .put(CommonPropertyType.TERMINATION, "none needed")
          .build();
  private static final String ENTRYFUNCTION_OPTION = "analysis.entryFunction";

  private static Set<SpecificationProperty> handlePropertyFile(Map<String, String> cmdLineOptions)
      throws InvalidCmdlineArgumentException {
    List<String> specificationFiles =
        Splitter.on(',')
            .trimResults()
            .omitEmptyStrings()
            .splitToList(cmdLineOptions.getOrDefault(SPECIFICATION_OPTION, ""));

    List<String> propertyFiles =
        specificationFiles.stream().filter(file -> file.endsWith(".prp")).collect(toList());
    if (propertyFiles.isEmpty()) {
      return ImmutableSet.of();
    }
    if (propertyFiles.size() > 1) {
      throw new InvalidCmdlineArgumentException("Multiple property files are not supported.");
    }
    String propertyFile = propertyFiles.get(0);

    // Parse property files
    PropertyFileParser parser = new PropertyFileParser(Paths.get(propertyFile));
    try {
      parser.parse();
    } catch (InvalidPropertyFileException e) {
      throw new InvalidCmdlineArgumentException(
          String.format("Invalid property file '%s': %s", propertyFile, e.getMessage()), e);
    } catch (IOException e) {
      throw new InvalidCmdlineArgumentException(
          "Could not read property file: " + e.getMessage(), e);
    }

    // set the file from where to read the specification automaton
    ImmutableSet<SpecificationProperty> properties =
        FluentIterable.from(parser.getProperties())
            .transform(
                prop ->
                    new SpecificationProperty(
                        parser.getEntryFunction(),
                        prop,
                        Optional.ofNullable(SPECIFICATION_FILES.get(prop))
                            .map(CmdLineArguments::resolveSpecificationFileOrExit)))
            .toSet();
    assert !properties.isEmpty();

    String specFiles =
        Optionals.presentInstances(
                properties
                    .stream()
                    .map(SpecificationProperty::getInternalSpecificationPath)
                    .distinct())
            .collect(Collectors.joining(","));
    cmdLineOptions.put(SPECIFICATION_OPTION, specFiles);
    if (cmdLineOptions.containsKey(ENTRYFUNCTION_OPTION)) {
      if (!cmdLineOptions.get(ENTRYFUNCTION_OPTION).equals(parser.getEntryFunction())) {
        throw new InvalidCmdlineArgumentException(
            "Mismatching names for entry function on command line and in property file");
      }
    } else {
      cmdLineOptions.put(ENTRYFUNCTION_OPTION, parser.getEntryFunction());
    }
    return properties;
  }

  @Options
  private static class WitnessOptions {
    @Option(
        secure = true,
        name = "witness.validation.file",
        description = "The witness to validate.")
    @FileOption(Type.OPTIONAL_INPUT_FILE)
    private @Nullable Path witness = null;

    @Option(
        secure = true,
        name = "witness.validation.violation.config",
        description =
            "When validating a violation witness, "
                + "use this configuration file instead of the current one.")
    @FileOption(Type.OPTIONAL_INPUT_FILE)
    private @Nullable Path violationWitnessValidationConfig = null;

    @Option(
        secure = true,
        name = "witness.validation.correctness.config",
        description =
            "When validating a correctness witness, "
                + "use this configuration file instead of the current one.")
    @FileOption(Type.OPTIONAL_INPUT_FILE)
    private @Nullable Path correctnessWitnessValidationConfig = null;
  }

  private static Configuration handleWitnessOptions(
      Configuration config, Map<String, String> overrideOptions)
      throws InvalidConfigurationException, IOException, InterruptedException {
    WitnessOptions options = new WitnessOptions();
    config.inject(options);
    if (options.witness == null) {
      return config;
    }

    WitnessType witnessType = AutomatonGraphmlParser.getWitnessType(options.witness);
    final Path validationConfigFile;
    switch (witnessType) {
      case VIOLATION_WITNESS:
        validationConfigFile = options.violationWitnessValidationConfig;
        String specs = overrideOptions.get(SPECIFICATION_OPTION);
        String witnessSpec = options.witness.toString();
        specs = specs == null ? witnessSpec : Joiner.on(',').join(specs, witnessSpec.toString());
        overrideOptions.put(SPECIFICATION_OPTION, specs);
        break;
      case CORRECTNESS_WITNESS:
        validationConfigFile = options.correctnessWitnessValidationConfig;
        overrideOptions.put(
            "invariantGeneration.kInduction.invariantsAutomatonFile", options.witness.toString());
        break;
      default:
        throw new InvalidConfigurationException(
            "Witness type " + witnessType + " of witness " + options.witness + " is not supported");
    }
    if (validationConfigFile == null) {
      throw new InvalidConfigurationException(
          "Validating (violation|correctness) witnesses is not supported if option witness.validation.(violation|correctness).config is not specified.");
    }
    return Configuration.builder()
        .loadFromFile(validationConfigFile)
        .setOptions(overrideOptions)
        .clearOption("witness.validation.file")
        .clearOption("witness.validation.violation.config")
        .clearOption("witness.validation.correctness.config")
        .clearOption("output.path")
        .clearOption("rootDirectory")
        .build();
  }

  @Options
  private static class BootstrapOptions {
    @Option(
        secure = true,
        name = "memorysafety.config",
        description =
            "When checking for memory safety properties, "
                + "use this configuration file instead of the current one.")
    @FileOption(Type.OPTIONAL_INPUT_FILE)
    private @Nullable Path memsafetyConfig = null;

    @Option(
        secure = true,
        name = "memorycleanup.config",
        description =
            "When checking for memory cleanup properties, "
                + "use this configuration file instead of the current one.")
    @FileOption(Type.OPTIONAL_INPUT_FILE)
    private @Nullable Path memcleanupConfig = null;

    @Option(
        secure = true,
        name = "overflow.config",
        description =
            "When checking for the overflow property, "
                + "use this configuration file instead of the current one.")
    @FileOption(Type.OPTIONAL_INPUT_FILE)
    private @Nullable Path overflowConfig = null;

    @Option(
        secure = true,
        name = "termination.config",
        description =
            "When checking for the termination property, "
                + "use this configuration file instead of the current one.")
    @FileOption(Type.OPTIONAL_INPUT_FILE)
    private @Nullable Path terminationConfig = null;

    @Option(
        secure = true,
        name = CmdLineArguments.PRINT_USED_OPTIONS_OPTION,
        description = "all used options are printed")
    private boolean printUsedOptions = false;
  }

  private static final ImmutableSet<? extends Property> MEMSAFETY_PROPERTY_TYPES =
      Sets.immutableEnumSet(
          CommonPropertyType.VALID_DEREF,
          CommonPropertyType.VALID_FREE,
          CommonPropertyType.VALID_MEMTRACK);
  private static final ImmutableMap<Property, TestTargetType> TARGET_TYPES =
      ImmutableMap.<Property, TestTargetType>builder()
          .put(CommonCoverageType.COVERAGE_BRANCH, TestTargetType.ASSUME)
          .put(CommonCoverageType.COVERAGE_CONDITION, TestTargetType.ASSUME)
          .put(CommonCoverageType.COVERAGE_ERROR, TestTargetType.ERROR_CALL)
          .put(CommonCoverageType.COVERAGE_STATEMENT, TestTargetType.STATEMENT)
          .build();

  private static Configuration handlePropertyOptions(
      Configuration config,
      BootstrapOptions options,
      Map<String, String> cmdLineOptions,
      Set<SpecificationProperty> pProperties)
      throws InvalidConfigurationException, IOException {
    Set<Property> properties =
        pProperties.stream().map(p -> p.getProperty()).collect(ImmutableSet.toImmutableSet());

    final Path alternateConfigFile;

    if (!Collections.disjoint(properties, MEMSAFETY_PROPERTY_TYPES)) {
      if (!MEMSAFETY_PROPERTY_TYPES.containsAll(properties)) {
        // Memsafety property cannot be checked with others in combination
        throw new InvalidConfigurationException(
            "Unsupported combination of properties: " + properties);
      }
      alternateConfigFile = check(options.memsafetyConfig, "memory safety", "memorysafety.config");
    } else if (properties.contains(CommonPropertyType.VALID_MEMCLEANUP)) {
      if (properties.size() != 1) {
        // MemCleanup property cannot be checked with others in combination
        throw new InvalidConfigurationException(
            "Unsupported combination of properties: " + properties);
      }
      alternateConfigFile =
          check(options.memcleanupConfig, "memory cleanup", "memorycleanup.config");
    } else if (properties.contains(CommonPropertyType.OVERFLOW)) {
      if (properties.size() != 1) {
        // Overflow property cannot be checked with others in combination
        throw new InvalidConfigurationException(
            "Unsupported combination of properties: " + properties);
      }
      alternateConfigFile = check(options.overflowConfig, "overflows", "overflow.config");
    } else if (properties.contains(CommonPropertyType.TERMINATION)) {
      // Termination property cannot be checked with others in combination
      if (properties.size() != 1) {
        throw new InvalidConfigurationException(
            "Unsupported combination of properties: " + properties);
      }
      alternateConfigFile = check(options.terminationConfig, "termination", "termination.config");
    } else if (properties.contains(CommonCoverageType.COVERAGE_ERROR)
        || properties.contains(CommonCoverageType.COVERAGE_BRANCH)
        || properties.contains(CommonCoverageType.COVERAGE_CONDITION)
        || properties.contains(CommonCoverageType.COVERAGE_STATEMENT)) {
      // coverage criterion cannot be checked with other properties in combination
      if (properties.size() != 1) {
        throw new InvalidConfigurationException(
            "Unsupported combination of properties: " + properties);
      }
      return Configuration.builder()
          .copyFrom(config)
          .setOption("testcase.targets.type", TARGET_TYPES.get(properties.iterator().next()).name())
          .build();
    } else {
      alternateConfigFile = null;
    }

    if (alternateConfigFile != null) {
      return Configuration.builder()
          .loadFromFile(alternateConfigFile)
          .setOptions(cmdLineOptions)
          .clearOption("memorysafety.config")
          .clearOption("memorycleanup.config")
          .clearOption("overflow.config")
          .clearOption("termination.config")
          .clearOption("output.disable")
          .clearOption("output.path")
          .clearOption("rootDirectory")
          .clearOption("witness.validation.file")
          .build();
    }
    return config;
  }

  private static Path check(Path config, String verificationTarget, String optionName)
      throws InvalidConfigurationException {
    if (config == null) {
      throw new InvalidConfigurationException(
          String.format(
              "Verifying %s is not supported if option %s is not specified.",
              verificationTarget, optionName));
    }
    return config;
  }

  public static class Config {

    public final Configuration configuration;

    public final String outputPath;

    public final Set<SpecificationProperty> properties;

    public final LogManager logManager;

    public Config(
        Configuration pConfiguration,
        String pOutputPath,
        Set<SpecificationProperty> pProperties,
        LogManager pLogManager) {
      configuration = pConfiguration;
      outputPath = pOutputPath;
      properties = ImmutableSet.copyOf(pProperties);
      logManager = pLogManager;
    }
  }
}
