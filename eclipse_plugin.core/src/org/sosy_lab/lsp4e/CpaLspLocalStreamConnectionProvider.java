package org.sosy_lab.lsp4e;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.eclipse.lsp4e.server.StreamConnectionProvider;
import org.sosy_lab.cpacheckerlsp.CPAcheckerLSP;

public class CpaLspLocalStreamConnectionProvider implements StreamConnectionProvider {
  private Future<?> launcherFuture = null;
  private InputStream in = null;
  private OutputStream out = null;
  private ServerSocket serverSocket = null;
  private Socket clientSocket = null;
  private Integer port;

  private static ExecutorService executor = null;

  public CpaLspLocalStreamConnectionProvider() {
    if (executor == null) {
    	executor = Executors.newCachedThreadPool();
    }
  }

  @Override
  public InputStream getErrorStream() {
    return null;
  }

  @Override
  public InputStream getInputStream() {
    return in;
  }

  @Override
  public OutputStream getOutputStream() {
    return out;
  }

  @Override
  public void start() throws IOException {
	  serverSocket = new ServerSocket(0);
	  port = serverSocket.getLocalPort();
	  executor.execute(new Runnable(){
		  @Override
          public void run() {
              CPAcheckerLSP.main(new String[]{port.toString()});
          }
	  });
      clientSocket = serverSocket.accept();
      in = clientSocket.getInputStream();
      out = clientSocket.getOutputStream();
  }

  @Override
  public void stop() {
    if (launcherFuture != null) launcherFuture.cancel(true);

    try {
      if (in != null) in.close();
      if (out != null) out.close();
    } catch (IOException e) { // TODO Auto-generated catch block
      e.printStackTrace();
    }

    out = null;
    in = null;
    serverSocket = null;
    clientSocket = null;
  }
}
