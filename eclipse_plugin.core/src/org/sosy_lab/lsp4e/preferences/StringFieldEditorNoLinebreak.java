package org.sosy_lab.lsp4e.preferences;

import org.eclipse.swt.widgets.Composite;

public class StringFieldEditorNoLinebreak extends org.eclipse.jface.preference.StringFieldEditor {
	
	public StringFieldEditorNoLinebreak(String name, String labelText, Composite parent) {
        super(name, labelText, UNLIMITED, VALIDATE_ON_KEY_STROKE, parent);
    }
	
	@Override
	protected boolean doCheckState() {
		String newline = System.getProperty("line.separator");
        if( getStringValue().contains(newline))
        {
        	return false;
        }
        return true;
    }
}
