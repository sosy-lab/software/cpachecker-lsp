package org.sosy_lab.lsp4e.preferences;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.service.prefs.Preferences;
import org.sosy_lab.cpacheckerlsp.IPreferenceConstants;

public class ConfigurationAdapter {
  private Preferences pref = null;
  private String cpacheckerSelection = null;
  private String machineModel = null;
  private boolean newJavaVm = false;

  private String localCmdline = null;

  public String getLocalCmdline() {
    localCmdline =
        getString(
            IPreferenceConstants.CPACHECKER_LOCAL_CMDLINE,
            IPreferenceConstants.CPACHECKER_LOCAL_CMDLINE_DEFAULT);
    return localCmdline;
  }

  public boolean shouldRunInNewJavaVm() {
    newJavaVm = getBoolean(IPreferenceConstants.CPACHECKER_NEW_JAVA_VM, true);
    return newJavaVm;
  }

  public String getCpacheckerSelection() {
    cpacheckerSelection =
        getString(
            IPreferenceConstants.CPACHECKER_SELECTION,
            IPreferenceConstants.CPACHECKER_SELECTION_LOCAL);
    return cpacheckerSelection;
  }

  public String getMachineModelSelection() {
	  machineModel =
        getString(
            IPreferenceConstants.CPACHECKER_MACHINEMODEL,
            IPreferenceConstants.CPACHECKER_MACHINEMODEL_32BIT);
    return machineModel;
  }
  public String getConfigurationString() {
    return getString(
        IPreferenceConstants.CPACHECKER_CONFIGURATION,
        IPreferenceConstants.CPACHECKER_CONFIGURATION_DEFAULT);
  }

  public String getSpecificationString() {
    return getString(
        IPreferenceConstants.CPACHECKER_SPECIFICATION,
        IPreferenceConstants.CPACHECKER_SPECIFICATION_DEFAULT);
  }

  public ConfigurationAdapter() {
    IPreferencesService service = Platform.getPreferencesService();
    IEclipsePreferences root = service.getRootNode();
    pref = root.node(InstanceScope.SCOPE).node(IPreferenceConstants.PREFERENCE_QUALIFIER);
    updateConfiguration();
  }

  private void updateConfiguration() {
    getCpacheckerSelection();
    getLocalCmdline();
    shouldRunInNewJavaVm();
    getSpecificationString();
    getConfigurationString();
  }

  /**
   * reads the value corresponding to key or default, and writes it to the configuration
   *
   * @param key key to read
   * @param def default in case not present yet
   * @return
   */
  private String getString(String key, String def) {
    String value = pref.get(key, def);
    pref.put(key, value);
    return value;
  }

  /**
   * reads the value corresponding to key or default, and writes it to the configuration
   *
   * @param key key to read
   * @param def default in case not present yet
   * @return
   */
  private boolean getBoolean(String key, boolean def) {
    boolean value = pref.getBoolean(key, def);
    pref.putBoolean(key, value);
    return value;
  }
}
