package org.sosy_lab.lsp4e.preferences;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.sosy_lab.cpacheckerlsp.IPreferenceConstants;
import org.sosy_lab.lsp4e.preferences.StringFieldEditorNoLinebreak;

public class MainPreferencePage extends FieldEditorPreferencePage
    implements IWorkbenchPreferencePage {

  public MainPreferencePage() {}

  public MainPreferencePage(int style) {
    super(style);
  }

  public MainPreferencePage(String title, int style) {
    super(title, style);
  }

  public MainPreferencePage(String title, ImageDescriptor image, int style) {
    super(title, image, style);
  }

  @Override
  public void init(IWorkbench workbench) {
    setPreferenceStore(
        new ScopedPreferenceStore(
            InstanceScope.INSTANCE, IPreferenceConstants.PREFERENCE_QUALIFIER));
  }

  @Override
  protected void createFieldEditors() {
    RadioGroupFieldEditor radioLocalOrCloud =
        new RadioGroupFieldEditor(
            IPreferenceConstants.CPACHECKER_SELECTION,
            "CPAchecker to use:",
            1,
            new String[][] {
              {
                IPreferenceConstants.CPACHECKER_SELECTION_LOCAL,
                IPreferenceConstants.CPACHECKER_SELECTION_LOCAL
              },
              {
                IPreferenceConstants.CPACHECKER_SELECTION_CLOUD,
                IPreferenceConstants.CPACHECKER_SELECTION_CLOUD
              }
            },
            getFieldEditorParent(),
            true);
    addField(radioLocalOrCloud);

    BooleanFieldEditor lspInNewJavaVm =
        new BooleanFieldEditor(
            IPreferenceConstants.CPACHECKER_NEW_JAVA_VM,
            "Start LSP Server in new Java VM",
            getFieldEditorParent());
    addField(lspInNewJavaVm);

    StringFieldEditorNoLinebreak configuration =
        new StringFieldEditorNoLinebreak(
            IPreferenceConstants.CPACHECKER_CONFIGURATION,
            "Configuration:",
            getFieldEditorParent());
    addField(configuration);
    
    StringFieldEditorNoLinebreak specification =
        new StringFieldEditorNoLinebreak(
            IPreferenceConstants.CPACHECKER_SPECIFICATION,
            "Specification:",
            getFieldEditorParent());
    addField(specification);
    
    RadioGroupFieldEditor x86orX64 =
            new RadioGroupFieldEditor(
                IPreferenceConstants.CPACHECKER_MACHINEMODEL,
                "Machine model:",
                1,
                new String[][] {
                  {
                    IPreferenceConstants.CPACHECKER_MACHINEMODEL_32BIT,
                    IPreferenceConstants.CPACHECKER_MACHINEMODEL_32BIT
                  },
                  {
                    IPreferenceConstants.CPACHECKER_MACHINEMODEL_64BIT,
                    IPreferenceConstants.CPACHECKER_MACHINEMODEL_64BIT
                  }
                },
                getFieldEditorParent(),
                true);
    addField(x86orX64);
        
    StringFieldEditorNoLinebreak additionalCmdLine =
        new StringFieldEditorNoLinebreak(
            IPreferenceConstants.CPACHECKER_LOCAL_CMDLINE,
            "Additional CPAchecker commandline (local only):",
            getFieldEditorParent());
    addField(additionalCmdLine);
  }
}
