package org.sosy_lab.lsp4e;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.eclipse.lsp4j.ConfigurationItem;
import org.eclipse.lsp4j.ConfigurationParams;
import org.eclipse.lsp4j.MessageParams;
import org.eclipse.lsp4j.MessageType;
import org.sosy_lab.cpacheckerlsp.IPreferenceConstants;
import org.sosy_lab.lsp4e.preferences.ConfigurationAdapter;

import eclipse_plugin.Activator;

@SuppressWarnings("restriction")
public class CpaCheckerLanguageClientImpl extends org.eclipse.lsp4e.LanguageClientImpl {

  public CpaCheckerLanguageClientImpl() {
    super();
  }

  @Override
  public CompletableFuture<List<Object>> configuration(ConfigurationParams configurationParams) {
    ConfigurationAdapter configmanager = Activator.getDefault().getConfigurationManager();
    String scopeUri = null;
    File configFile;
    List<Object> configlist = new ArrayList<Object>();

    if (configurationParams.getItems().size() > 0) {
      scopeUri = configurationParams.getItems().get(0).getScopeUri();
    } else {
      return CompletableFuture.supplyAsync(() -> configlist);
    }

    try {
      configFile = new File(new URI(scopeUri + "cpals.cfg"));
    } catch (URISyntaxException e) {
      this.logMessage(
          new MessageParams(
              MessageType.Error,
              "Configuration path from Server could not be parsed to URI: " + scopeUri));
      return CompletableFuture.supplyAsync(() -> configlist);
    }
    PrintWriter writer = null;
    try {
      writer = new PrintWriter(configFile, "UTF-8");
    } catch (FileNotFoundException e) {
    	 this.logMessage(
    	          new MessageParams(
    	              MessageType.Error,
    	              "Configuration path from Server could not be opened: " + configFile.getAbsolutePath()));
    	      return CompletableFuture.supplyAsync(() -> configlist);
    } catch (UnsupportedEncodingException e) {
    	// this should never happen
      e.printStackTrace();
    }

    for (ConfigurationItem item : configurationParams.getItems()) {
      String section = item.getSection();
      String value;
      switch (section) {
        case IPreferenceConstants.CPACHECKER_LOCAL_CMDLINE:
          value = configmanager.getLocalCmdline();
          configlist.add(value);
          writer.println(String.format("%s = %s", section, value));
          break;
        case IPreferenceConstants.CPACHECKER_SELECTION:
          value = configmanager.getCpacheckerSelection();
          configlist.add(value);
          writer.println(String.format("%s = %s", section, value));
          break;

        case IPreferenceConstants.CPACHECKER_CONFIGURATION:
          value = configmanager.getConfigurationString();
          configlist.add(value);
          writer.println(String.format("%s = %s", section, value));
          break;

        case IPreferenceConstants.CPACHECKER_SPECIFICATION:
          value = configmanager.getSpecificationString();
          configlist.add(value);
          writer.println(String.format("%s = %s", section, value));
          break;

        case IPreferenceConstants.CPACHECKER_MACHINEMODEL:
          value = configmanager.getMachineModelSelection();
          configlist.add(value);
          writer.println(String.format("%s = %s", section, value));
          break;
      }
    }
    writer.close();
    return CompletableFuture.supplyAsync(() -> configlist);
    // return  CompletableFuture.completedFuture(configlist);
  }
}
