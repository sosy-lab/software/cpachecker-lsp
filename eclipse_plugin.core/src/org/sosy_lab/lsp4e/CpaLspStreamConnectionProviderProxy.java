package org.sosy_lab.lsp4e;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.lsp4e.server.StreamConnectionProvider;
import org.sosy_lab.lsp4e.preferences.ConfigurationAdapter;

import eclipse_plugin.Activator;

public class CpaLspStreamConnectionProviderProxy implements StreamConnectionProvider {
  StreamConnectionProvider providerInterface;

  public CpaLspStreamConnectionProviderProxy() {
	if(Activator.getDefault().getConfigurationManager().shouldRunInNewJavaVm()) {
		providerInterface = new CpaLspProcessStreamConnectionProvider();
	}else {
	    providerInterface = new CpaLspLocalStreamConnectionProvider();
	}
  }

  @Override
  public InputStream getErrorStream() {
    return providerInterface.getErrorStream();
  }

  @Override
  public InputStream getInputStream() {
    return providerInterface.getInputStream();
  }

  @Override
  public OutputStream getOutputStream() {
    return providerInterface.getOutputStream();
  }

  @Override
  public void start() throws IOException {
    providerInterface.start();
  }

  @Override
  public void stop() {
    providerInterface.stop();
  }
}
