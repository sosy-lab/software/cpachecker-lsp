package org.sosy_lab.lsp4e;

import org.eclipse.cdt.ui.text.c.hover.ICEditorTextHover;
import org.eclipse.lsp4e.operations.hover.LSBasedHover;
import org.eclipse.ui.IEditorPart;

@SuppressWarnings("restriction")
public class CEditorTextHover extends LSBasedHover implements ICEditorTextHover {
  private IEditorPart editor;

  @Override
  public void setEditor(IEditorPart editor) {
    this.editor = editor;
  }
}
