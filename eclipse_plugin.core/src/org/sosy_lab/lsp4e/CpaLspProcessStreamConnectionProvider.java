package org.sosy_lab.lsp4e;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.lsp4e.server.ProcessStreamConnectionProvider;
import org.osgi.framework.Bundle;
import org.sosy_lab.cpacheckerlsp.LSPClassLoader;

import eclipse_plugin.Activator;

public class CpaLspProcessStreamConnectionProvider extends ProcessStreamConnectionProvider {
  public CpaLspProcessStreamConnectionProvider() {
    Activator.getDefault();
  Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
    org.eclipse.core.runtime.Path serverPath = new org.eclipse.core.runtime.Path("lib/server.jar");
    org.eclipse.core.runtime.Path cpacheckerPath = new org.eclipse.core.runtime.Path("");
    URL serverPathURL = FileLocator.find(bundle, serverPath, null);
    URL cpacheckerPathURL = FileLocator.find(bundle, cpacheckerPath, null);
    URL resolvedServerPathURL = null;
    URL resolvedCpacheckerPathURL = null;
    try {
    	resolvedServerPathURL = FileLocator.resolve(serverPathURL);
    	resolvedCpacheckerPathURL = FileLocator.resolve(cpacheckerPathURL);
    } catch(IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    String serverLocation = null;
    String cpacheckerLocation = null;
    try {
      serverLocation = Paths.get(resolvedServerPathURL.toURI()).toString();
      cpacheckerLocation = Paths.get(resolvedCpacheckerPathURL.toURI()).toString();
    } catch (URISyntaxException e) { // TODO Auto-generated catch block
      e.printStackTrace();
    }
    String launcherClassName = LSPClassLoader.class.getCanonicalName();
    List<String> commands = new ArrayList<String>();
    commands.add(System.getProperty("java.home")+"/bin/java");
    //commands.add("-Xdebug");
    //commands.add("-Xrunjdwp:transport=dt_socket,address=1044,server=y,suspend=y");
    commands.add("-cp");
    commands.add(serverLocation);
    commands.add(launcherClassName);
    setCommands(commands);
    setWorkingDirectory(cpacheckerLocation);
  }
}
