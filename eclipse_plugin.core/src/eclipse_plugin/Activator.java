package eclipse_plugin;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.sosy_lab.lsp4e.preferences.ConfigurationAdapter;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "eclipse_plugin.core"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	private ConfigurationAdapter configManager;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		this.configManager = new ConfigurationAdapter();
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	public ConfigurationAdapter getConfigurationManager()
	{
		return configManager;
	}

}
