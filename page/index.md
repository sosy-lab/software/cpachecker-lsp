﻿
# CPAchecker  LSP
This is the update site for CPAchecker LSP.
CPAchecker LSP enables formal verification of C files from within the Eclipse CDT IDE. 

## Installation
- Your Eclipse Version needs to be at least 2019-09
- Start your Eclipse IDE (with installed CDT) with at least Java 11.
- Go to "Help->Install New Software"
- Click on "Add..." to add the update site for CPAchecker LSP, enter  https://sosy-lab.gitlab.io/software/cpachecker-lsp/ into the "Location" field, and finish by clicking "Add".
- Select the new update site in the drop-down menu at the top
- Select "CPAchecker LSP Eclipse Feature" in the list
- Make sure that the checkbox "Contact all update sites during install to find required software" is ticked.
- Press "Next>" and follow the instructions.
- Restart Eclipse when asked.
## Configuration
To see the configuration options for CPAchecker LSP, go to "Window->Preferences" and select "CPAchecker" from the list
![Configuration settings for CPAchecker LSP](https://sosy-lab.gitlab.io/software/cpachecker-lsp/configuration_settings.png)
### Available Options:
- You can chose if the verification tasks will be done locally or by sending a request to the VerifierCloud at https://vcloud.sosy-lab.org/cpachecker/webclient/help/
- The option "Start LSP Server in new Java VM" should stay selected for normal use. Disabling it is mostly useful for debugging.
- You can chose a configuration file for CPAchecker. Any of the configurations shipped with CPAchecker by default should work.
- You can chose a specification file for CPAchecker. Any of the specifications shipped with CPAchecker by default should work.
- If you selected to use local verification, you can add additional commandline arguments to verification tasks. By default, this includes "-preprocess".

## First Steps
- For this example to behave as described, set the used CPAchecker Configuration to "default", and leave the CPAchecker  Specification empty
- Create a new C Project
- Download https://raw.githubusercontent.com/sosy-lab/cpachecker/trunk/doc/examples/example.c and add it to your project
- Open "example.c", change something minor and save the file. Adding or removing a new line should be enough.
- Changing and saving a file triggers a verification task.
- You will see the progress and the result in the console window. 
- Now try adding "goto ERROR;" in line 12, and save
- A new verification task should have been started. This time, the verification result should be "false", and you should see that the beginning of line 12 has a red underline, signifying the error.
![Example of a found property violation](https://sosy-lab.gitlab.io/software/cpachecker-lsp/error_example.png)
- Now refresh your project in the project explorer. A new folder named "cparesults" should have become visible. It contains more detailed information about the last verification run for each file.
![The cparesults directory inside the project contains all CPAchecker result files](https://sosy-lab.gitlab.io/software/cpachecker-lsp/results_example.png)

## Known Issues
- Eclipse has to be run with at least Java 11. As the plugin directly uses CPAchecker classes and CPAchecker requires Java 11, the plugin also requires Java 11 to run.

- The plugin will not work on *.c files that are automatically reopened by Eclipse after a IDE restart. You have to close the editor and open the file again. This is an issue with the CDT editor for .c files. It does not occour with the generic editor.

- After getting results, refresh your project in Eclipse to make the result directory appear in your project explorer.

- If you are getting a Message "Submitting run configuration timed out after 15 seconds" when using cloud verification, this is not a problem of CPAchecker LSP but of VerifierCloud failing to give a response code when the most current CPAchecker is not compiled on the VerifierCloud.
