How to compile and install:
1. clone this repository
2. execute "setup-repo.sh" 
	this downloads the newest version of CPAchecker from the VerifierCloud (https://vcloud.sosy-lab.org/cpachecker/webclient/help/), registers the libraries with maven, and copies the them to the right location for the build process
3. build with "./mvnw clean verify"
4. add the newly built update site (like "path-to-repo/eclipse_plugin.site/target/repository/") to your Eclipse installation
5. install via Eclipse

How to use:
1. open c source file with eclipse
2. CPAchecker is executed as configured when the file is saved


This project is built using Eclipse Tycho (https://www.eclipse.org/tycho/) and requires at least maven 3.0 (http://maven.apache.org/download.html) to be built via CLI. 

The first run will take quite a while since maven will download all the required dependencies in order to build everything.

In order to use the generated eclipse plugins in Eclipse, you will need m2e (https://www.eclipse.org/m2e) 
and the m2eclipse-tycho plugin (https://github.com/tesla/m2eclipse-tycho/). Update sites to install these plugins : 

* m2e stable update site : http://download.eclipse.org/technology/m2e/releases/
* m2eclipse-tycho dev update site : http://repo1.maven.org/maven2/.m2e/connectors/m2eclipse-tycho/0.7.0/N/0.7.0.201309291400/

